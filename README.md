# Dev. Front-End

Sua tarefa é construir um aplicativo para a aplicação BlogPosts. A aplicação é um simples crud de posts de blog, com seus títulos e suas descrições. Utilize um repositório Git (público, de preferência) para versionamento e disponibilização do código.

O aplicativo deve ser construído em React Native, utilizando o protótipo disponibilizado no [FIGMA](https://www.figma.com/file/mw4CM9fNLXggwFogKoheXx/Teste?node-id=1%3A4).

## O que será avaliado

Queremos avaliar sua capacidade de desenvolver e documentar a aplicação. Serão avaliados:

-   Código bem escrito e limpo;
-   Quais ferramentas foram usadas, como e porquê;
-   Seu conhecimento na construção de componentes reutilizáveis, consumo de APIs REST;
-   Sua capacidade de se comprometer com o que foi fornecido (wireframe, styleguide);
-   Sua capacidade de documentação da sua parte da aplicação.

## O mínimo necessário

- As telas seguindo o protótipo do figma a seguir e utilizando a API disponibilizada ao fim deste documento;
- README.md contendo informações básicas do projeto e como executá-lo.
-   **Typescript**;
-   **Gerenciamento de estado com Context API**;
-   **React Hooks**;
-   **Animação nas micros interações**;


## Bônus

Os seguintes itens não são obrigatórios, mas darão mais valor ao seu trabalho (os em negrito são mais significativos para nós e serão considerados um diferencial):

-   Uso de ferramentas ou bibliotecas externas que facilitem o seu trabalho;
-   Cuidados especiais com otimização, padrões, entre outros;
-   **Testes Unitários**;
-   **Offline first**;
-   **Documentação dos componentes utilizando ferramentas como Storybook**;
-   Sugestões sobre o desafio embasadas em alguma argumentação.

# User Stories

## 1: O(A) usuário(a) deve poder ver a lista de todos os posts cadastrados e adicionar um novo post

Listar posts:

`GET /posts`

Adicionar novo post:

`POST /posts Content-Type: application/json`

```json
{
    "title": "foo",
    "body": "bar",
    "userId": 1
}
```


## 2: O(A) usuário(a) deve poder clicar em remover um post e o post ser removido da listagem

Remover um post:

`DELETE /posts/{id}`


## 3: O(A) usuário(a) deve poder utilizar o input de busca para realizar uma busca por um post

Buscar pelo `title` de um post:

`GET /posts?title=qui%20est%20esse`


## 4: O(A) usuário(a) deve poder favoritar um post, a api não disponibiliza rota para favoritar

===============================================================================================

## Critérios de Aceitação

-   O aplicativo deve ser codificado seguindo:
    - Os fluxos definidos no protótipo do FIGMA (listar, adicionar, remover, buscar e favoritar);
    - A estilização dos componentes necessários, tipografia, e espaçamentos de acordo com o protótipo do [FIGMA](https://www.figma.com/file/mw4CM9fNLXggwFogKoheXx/Teste?node-id=1%3A4)
    - A API disponibilizada logo abaixo.

# API

A base url a ser utilizada será esta: `https://jsonplaceholder.typicode.com`.

Por exemplo, para listar os posts deve ser feita uma requisição para: `https://jsonplaceholder.typicode.com/posts`.

E assim por diante seguindo os endpoints demonstrados acima.

## Importante

Esta é uma fake api, se for adicionado um post, por exemplo, o post não é persistido no servidor, mas é retornado uma `response` para que o post seja adicionado na listagem. O mesmo deve ser lembrado para remoção.

OKAYDEV deseja um Bom trabalho!